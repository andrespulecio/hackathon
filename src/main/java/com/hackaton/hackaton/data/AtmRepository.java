package com.hackaton.hackaton.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AtmRepository extends MongoRepository<ProductoATM, String> {

    // Se garantiza que retorna un Unico elemento
    @Query("{'atm':?0}")
    public ProductoATM findByAtm(String atm);

    @Query("{'marca':?0}")
    public List<ProductoATM> findByMarca(String marca);

    @Query("{ 'VersAplicativo': ?0,'marca': {$ne:?1} }")
    public List<ProductoATM> findByVerMa(String vers, String marca);

    @Query("{ 'nombre': {$regex:?0} }")
    public List<ProductoATM> findByNombreC(String nombre);

    @Query("{'nombre':?0}")
    public  List<ProductoATM> findByNombre(String nombre);

    @Query("{$and:[{'marca':?0 },{'modelo':?1 }]}")
    public List<ProductoATM> findByMarcayModelo(String marca, String modelo);

}
