package com.hackaton.hackaton;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class CabeceraController {

    @GetMapping("/saludar")
    public ResponseEntity<String> saludar(@RequestHeader("accept-languaje") String idioma){

        switch (idioma){
            case "ES":
                return new ResponseEntity<String>("Bienvenidos a nuestra API --> Grupo 5", HttpStatus.OK);
            case "EN":
                return new ResponseEntity<String>("Welcome to our API --> Group 5", HttpStatus.OK);
            case "":
                return new ResponseEntity<String>("Idioma no detectado", HttpStatus.OK);
        }
        return new ResponseEntity<String>(idioma, HttpStatus.OK);
    }
 }

