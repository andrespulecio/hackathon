FROM openjdk:17-slim-buster
COPY target/hackaton-0.0.1.jar app.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/app.jar"]