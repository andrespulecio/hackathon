package com.hackaton.hackaton;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackatonApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HackatonApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Preparando Mongo");
	}
}
