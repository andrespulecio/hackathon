# HACKATHON #

Este repositorio es el ejercicio del grupo 5 en el Practitioner Backend. 

### ¿Para qué es este repositorio? ###

* API que resuelve una manualidad para el manejo de los ATM.

### Herramientas ###
* Java jdk-15 ( [https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html](https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html) )
* MongoDB ( [https://www.mongodb.com/es](https://www.mongodb.com/es) )
* Maven ( [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi) )

### ¿Cómo configuro? ###
* Ejecutando
```bash
mvn spring-boot:run
```
* Usando Docker descargando la imagen 
```bash
docker push andrespulecio/api-hackathon:tagname
```
y ejecutando
```bash
docker run -p 8083:8083 andrespulecio/api-hackathon
```

### Visualizar ###

Se expone en [locahost:8083](http://locahost:8083/)

