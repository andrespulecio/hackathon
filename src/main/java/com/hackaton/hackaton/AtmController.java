package com.hackaton.hackaton;

import com.hackaton.hackaton.data.AtmRepository;
import com.hackaton.hackaton.data.ProductoATM;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
public class AtmController {

    @Autowired
    private AtmRepository repository;

    /* Get lista total ATMs */
    @GetMapping(value = "/ATM", produces = "application/json")
    @ApiOperation(value="Lista ATMs",notes = "Este metodo Obtiene la lista total de ATM")
    public ResponseEntity<List<ProductoATM>> obtenerListado() {
        List<ProductoATM> lista = repository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    /* Get Unico ATM */
    @GetMapping(value = "/ATM/{atm}", produces = "application/json")
    @ApiOperation(value="Obtiene un único ATM",notes = "Este metodo Obtiene un Atm enviado por path")
    public ResponseEntity<ProductoATM> obtenerProductoPorATM(@PathVariable String atm)
    {
        Optional<ProductoATM> resultado = Optional.ofNullable(repository.findByAtm(atm));
        if (resultado.isPresent()) {
            return new ResponseEntity(resultado, HttpStatus.OK);
        }
        return new ResponseEntity("Numero de Cajero no encontrado", HttpStatus.NOT_FOUND);
    }
    /*Get ATM por Marca*/
    @GetMapping(value = "/ATMmarca/{marca}", produces = "application/json")
    @ApiOperation(value="Lista ATMs por Marca",notes = "Este metodo Obtiene la lista total de ATM segun la marca")
    public ResponseEntity<List<ProductoATM>> obtenerProductoPorMarca(@PathVariable String marca)
    {
        List<ProductoATM> resultado = null;
        ResponseEntity respuesta = null;
        try{
            resultado = (List<ProductoATM>) repository.findByMarca(marca);
            if (resultado==null || resultado.isEmpty()) {
                respuesta = new ResponseEntity<>("marca no encontrada", HttpStatus.NOT_FOUND);
            }
            else {
                respuesta = new ResponseEntity<List<ProductoATM>>(resultado, HttpStatus.OK);
            }
        }
        catch (Exception ex){
            respuesta = new ResponseEntity<>("marca no encontrada", HttpStatus.NOT_FOUND);
        }
        return respuesta;
     }

    /*Get Producto por Nombre */
    @GetMapping(value = "/productosbynombre/{nombre}", produces = "application/json")
    @ApiOperation(value="Lista ATMs por nombre",notes = "Este metodo Obtiene la lista total de ATM por nombre exacto")
    public ResponseEntity<List<ProductoATM>> obtenerProductoPorNombre(@PathVariable String nombre) {
        List<ProductoATM> resultado = (List<ProductoATM>) repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoATM>>(resultado, HttpStatus.OK);
    }

    @GetMapping("/productosbyid/{id}")
    @ApiOperation(value="Obtiene un único ATM por ID",notes = "Este metodo Obtiene un ATM por su ID")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable String id) {
        Optional<ProductoATM> resultado = repository.findById(String.valueOf(id));
        if (resultado.isPresent()) {
            return new ResponseEntity(resultado, HttpStatus.OK);
        }
        return  new ResponseEntity<String>("ID de ATM no encontrado", HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/prodbymarcaymodelo/{marca}/{modelo}", produces = "application/json")
    @ApiOperation(value="Lista ATMs por Marca y Modelo",notes = "Este metodo Obtiene ATMs según la marca y el modelo")
    public ResponseEntity<List<ProductoATM>> getAtmMarcaModelo(@PathVariable String marca, @PathVariable String modelo) {
        List<ProductoATM> resultado = (List<ProductoATM>) repository.findByMarcayModelo(marca, modelo);
        return new ResponseEntity<List<ProductoATM>>(resultado, HttpStatus.OK);
    }

     /*Get ATM por VersionAplicativo y Marca*/
    @GetMapping(value = "/ATMmarca/{vers}/{marca}", produces = "application/json")
    @ApiOperation(value="Lista ATMs por Marca y Version",notes = "Este metodo Obtiene ATMs según la marca y la version")
    public ResponseEntity<List<ProductoATM>> obtenerProductoPorVersMarca(@PathVariable String vers, @PathVariable String marca)
    {
        List<ProductoATM> resultado = null;
        ResponseEntity respuesta = null;
        resultado = (List<ProductoATM>) repository.findByVerMa(vers,marca);
        if (resultado==null || resultado.isEmpty()) {
            respuesta = new ResponseEntity<>("Datos no validos", HttpStatus.NOT_FOUND);
        }
        else {
            respuesta = new ResponseEntity<List<ProductoATM>>(resultado, HttpStatus.OK);
        }
        return respuesta;
    }
    /*Get ATM por coincidencia de nombre*/
    @GetMapping(value = "/ATMnombre/{nombre}", produces = "application/json")
    @ApiOperation(value="Lista ATMs por nombre V2",notes = "Este metodo Obtiene la lista total de ATM por Coincidencia en Nombre")
    public ResponseEntity<List<ProductoATM>> obtenerAtmPorNombreC(@PathVariable String nombre)
    {
        List<ProductoATM> resultado = null;
        ResponseEntity respuesta = null;
        resultado = (List<ProductoATM>) repository.findByNombreC(nombre);
        if (resultado==null || resultado.isEmpty()) {
            respuesta = new ResponseEntity<>("Datos no validos", HttpStatus.NOT_FOUND);
        }
        else {
            respuesta = new ResponseEntity<List<ProductoATM>>(resultado, HttpStatus.OK);
        }
        return respuesta;
    }
    /*Actualiza VersAplicativo ATM */
    @PutMapping(value="/ATM/{atm}")
    @ApiOperation(value="Realiza modificacion de los valores Version, JAM y Sistema",
                  notes = "Este metodo permite la modificación especificando en el Json los 3 campos a la vez o solo uno de ellos ")
    public ResponseEntity<String> updateVersion(@PathVariable String atm, @RequestBody ProductoATM productoAtm)
    {
        Optional<ProductoATM> resultado = Optional.ofNullable(repository.findByAtm(atm));
        if (resultado.isPresent()) {
            ProductoATM atmAModificar = resultado.get();

            if (productoAtm.VersAplicativo != null){
                atmAModificar.VersAplicativo = productoAtm.VersAplicativo;
            }
            if (productoAtm.JAM != null){
                atmAModificar.JAM = productoAtm.JAM;
            }
            if (productoAtm.Sistema != null){
                atmAModificar.Sistema = productoAtm.Sistema;
            }
            ProductoATM guardado = repository.save(resultado.get());
        //  return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
            return new ResponseEntity(resultado, HttpStatus.OK);
        }
        return new ResponseEntity<String>("ATM no encontrado", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/ATM/{atm}")
    @ApiOperation(value="Elimina un ATM",notes = "Este metodo elimina un ATM, formato atmxxxx")
    public ResponseEntity<String> deleteProducto(@PathVariable String atm) {
        Optional<ProductoATM> resultado = Optional.ofNullable(repository.findByAtm(atm));
        if (resultado.isPresent()) {
            ProductoATM productoAEliminar = resultado.get();
            repository.deleteById(productoAEliminar.id);
            return new ResponseEntity<String>("Cajero Automatico Eliminado", HttpStatus.OK);
        }
        return new ResponseEntity<String>("ATM no encontrado", HttpStatus.NOT_FOUND);
    }


}
