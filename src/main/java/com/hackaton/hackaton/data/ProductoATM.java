package com.hackaton.hackaton.data;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Hackathon")
public class ProductoATM {

    @Id
    public String id;
    public String atm;
    public String nombre;
    public String marca;
    public String modelo;
    public String so;
    public String tipo;
    public String IP;
    public String VersAplicativo;
    public String JAM;
    public String Sistema;

    public ProductoATM() {
    }

    public ProductoATM(String atm, String nombre, String marca, String modelo, String so, String tipo, String IP, String versAplicativo, String JAM, String sistema) {
        this.atm = atm;
        this.nombre = nombre;
        this.marca = marca;
        this.modelo = modelo;
        this.so = so;
        this.tipo = tipo;
        this.IP = IP;
        this.VersAplicativo = versAplicativo;
        this.JAM = JAM;
        this.Sistema = sistema;
    }

    @Override
    public String toString() {
        return "ProductoATM{" +
                "id='" + id + '\'' +
                ", atm='" + atm + '\'' +
                ", nombre='" + nombre + '\'' +
                ", marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", so='" + so + '\'' +
                ", tipo='" + tipo + '\'' +
                ", IP='" + IP + '\'' +
                ", VersAplicativo='" + VersAplicativo + '\'' +
                ", JAM='" + JAM + '\'' +
                ", Sistema='" + Sistema + '\'' +
                '}';
    }

}
