package com.hackaton.hackaton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket apiDocket(){
        //Detalle de los metodos de la api
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hackaton.hackaton"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo () {
        return new ApiInfo("API Cajeros Electronicos",
                "API que gestiona la configuracion de Cajeros Electronicos",
                "0.0.1",
                "https://www.bbva.com.co/",
                new Contact("Grupo 5",
                              "https://meet.google.com/zdt-myyv-hai",
                            "grupo5@hackaton.com"),
                "LICENCIA HACKATHON",
                "https://www.bbva.com.co/",
                Collections.emptyList());
    }

}

